package com.hacktory.soyawars;

import java.util.ArrayList;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class DrogaArrayAdapter  extends ArrayAdapter<Droga>{
	
	private final Context context;
	private final ArrayList<Droga> drogaArrayList;
	private int position;
	
	public DrogaArrayAdapter(Context context, ArrayList<Droga> drogaArrayList){
		
		super(context, R.layout.item_droga, drogaArrayList);
		Log.i("JOSE", getClass().getSimpleName() + ":setting up");
		this.context = context;
		this.drogaArrayList = drogaArrayList;
		
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		//
		Log.i("JOSE", getClass().getSimpleName() + ":en getview");
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		//
		Log.i("JOSE", getClass().getSimpleName() + ":en getview after inflate");
		View rowView = null;
		rowView = inflater.inflate(R.layout.item_droga, parent, false);
		Log.i("JOSE", getClass().getSimpleName() + ":after rowview1");
		
		TextView cantidad = (TextView) rowView.findViewById(R.id.cantdroga);
		TextView nombre = (TextView) rowView.findViewById(R.id.tipodroga);
		TextView precio = (TextView) rowView.findViewById(R.id.preciodroga);
		RelativeLayout lay = (RelativeLayout) rowView.findViewById(R.id.itemdrog);
		
		Log.i("JOSE", getClass().getSimpleName() + ":los agarro?");
		
		cantidad.setText(drogaArrayList.get(position).getCantidad());
		Log.i("JOSE", getClass().getSimpleName() + ":en cantidad");
		nombre.setText(drogaArrayList.get(position).getNombre());
		Log.i("JOSE", getClass().getSimpleName() + ":en nombre");
		precio.setText(drogaArrayList.get(position).getPrecioFormato());
		Log.i("JOSE", getClass().getSimpleName() + ":bef return");
		
		this.position = position;
		
		/*lay.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Toast.makeText(getContext(), "aja"+ drogaArrayList.get(getPosition()).getCantidad(), Toast.LENGTH_SHORT).show();
				
			}
		});*/
		
		return rowView;
		
	}
	
	
	
	
	public int getPosition(){
		return this.position;
	}
	
}
