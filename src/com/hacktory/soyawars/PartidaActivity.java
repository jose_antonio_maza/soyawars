package com.hacktory.soyawars;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.os.Build;

public class PartidaActivity extends Activity {
	final Context context = this;
	public static String[] mArrayDrogas;
	public Partida juego;
	TextView lblciudad, dia, deuda, pisto;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		mArrayDrogas = getResources().getStringArray(R.array.drogas);
		Log.i("JOSE", getClass().getSimpleName() + ":entered partida");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.partida);
		juego = new Partida();
		lblciudad = (TextView) findViewById(R.id.ciudad);
		dia = (TextView) findViewById(R.id.dia);
		deuda = (TextView) findViewById(R.id.deuda);
		pisto = (TextView) findViewById(R.id.pisto);
		
		lblciudad.setText("Centro");
		deuda.setText(this.juego.getDeuda());
		pisto.setText(this.juego.getPisto());
		
		//this.juego.setCiudad("Centro");
		
		Button coaster = (Button) findViewById(R.id.btnMover); 
		
		
		coaster.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
			
				final Dialog dialog = new Dialog(context);
				dialog.setContentView(R.layout.coaster_mapa);
				dialog.setTitle("Elige una ruta...");
	 
				// set the custom dialog components - text, image and button
				//ImageView satelite = (ImageView) dialog.findViewById(R.id.imagensansal);
				//text.setText("Android custom dialog example!");
				//image.setImageResource(R.drawable.ic_launcher);
	 
				Button soya = (Button) dialog.findViewById(R.id.soya);
				Button metro = (Button) dialog.findViewById(R.id.metro);
				Button centro = (Button) dialog.findViewById(R.id.centro);
				Button apopa = (Button) dialog.findViewById(R.id.apopa);
				Button tecla = (Button) dialog.findViewById(R.id.tecla);
				Button oriente = (Button) dialog.findViewById(R.id.oriente);

				Button ir = (Button) dialog.findViewById(R.id.ir_button);
				// if button is clicked, close the custom dialog
				soya.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						dialog.setTitle("A Soya!");
						actualizarJuego("Soyapango");
						//dialog.dismiss();
					}
				});
				
				centro.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						
						dialog.setTitle("Al Centrooou!");
						actualizarJuego("Centro");
						//dialog.dismiss();
					}
				});
				
				metro.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						dialog.setTitle("Metro! Metro!");
						actualizarJuego("MetroCentro");
						//dialog.dismiss();
					}
				});
				
				apopa.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						dialog.setTitle("Apopaaaa!");
						actualizarJuego("Apopa");
						//dialog.dismiss();
					}
				});
				
				oriente.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						dialog.setTitle("Orienteee!");
						actualizarJuego("Terminal de Oriente");
						//dialog.dismiss();
					}
				});
				
				tecla.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						dialog.setTitle("Tecla!...Vamos!");
						actualizarJuego("Santa Tecla");
						//dialog.dismiss();
					}
				});
				
				ir.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						dialog.dismiss();
						
					}
				});
	 
				dialog.show();
			
			}
				
			
		});	
		
	}
	public void actualizarPisto(Double pisto){
		this.juego.setPisto(pisto);
		TextView money = (TextView) findViewById(R.id.pisto);
		money.setText(this.juego.getPisto());
	}
	public void actualizarJuego(String ciudad){
		this.juego.setCiudad(ciudad);
		this.juego.setDia(this.juego.getIntDia()+1);
		
		
		DrogasFragment fragment =  ((DrogasFragment)getFragmentManager().findFragmentById(R.id.fragment1));
				fragment.actualizarYGenerarLista();
		
		

		lblciudad = (TextView) findViewById(R.id.ciudad);
		dia = (TextView) findViewById(R.id.dia);
		
		
		lblciudad.setText(this.juego.getCiudad());
		dia.setText(this.juego.getDia());
		
		
	}
	
	@Override
	public void onResume(){
		super.onResume();
		
	}
	
	@Override
	public void onPause(){
		super.onPause();
	}
	

	

	

}
