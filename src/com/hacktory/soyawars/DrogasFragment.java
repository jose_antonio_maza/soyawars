package com.hacktory.soyawars;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

public class DrogasFragment  extends ListFragment{
	private ArrayList<Droga> misDrogas;
	private Droga nuevaDroga;
	private View ventana;
	private int position;
	private ListaDrogas ld;
	private SeekBar seekbar1;
	private TextView valorSeek;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		return super.onCreateView(inflater, container, savedInstanceState);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		Log.i("JOSE", getClass().getSimpleName() + ":antes de lis");
		getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		
		ld = new ListaDrogas(); 
		misDrogas = ld.generarDrogas();
		
		// Setea el list adapter para la ListView  
		Log.i("JOSE", getClass().getSimpleName() + ":antes de droga");
		setListAdapter(new DrogaArrayAdapter(getActivity(),misDrogas));
		//if (-1 != mCurrIdx)
		//	getListView().setItemChecked(mCurrIdx, true);
		//setListAdapter(new ArrayAdapter<String>(getActivity(),
		//		R.layout.item_droga,R.id.tipodroga, Partida.mArrayDrogas));
		
	}
	
	@Override
	public void onListItemClick(ListView l, View v, int position, long id){
		
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setMessage("ugh")
		.setTitle("Comprar "+misDrogas.get(position).getNombre() + "?");
		this.position = position;
		builder.setPositiveButton("Si", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				LayoutInflater inflater = getActivity().getLayoutInflater();
				AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
				builder.setMessage("ugh")
				.setTitle("Cuanto va a querer jefe?");
				ventana = inflater.inflate(R.layout.compra, null);
				seekbar1 = (SeekBar) ventana.findViewById(R.id.seekBar1);
				seekbar1.setMax(getMaximoComprable(misDrogas.get(DrogasFragment.this.position)));
				valorSeek = (TextView) ventana.findViewById(R.id.valorBarra);
				seekbar1.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
					
					@Override
					public void onProgressChanged(SeekBar seekBar, int progress,
							boolean fromUser) {
						valorSeek.setText(Integer.toString(progress));
						
					}

					@Override
					public void onStartTrackingTouch(SeekBar seekBar) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void onStopTrackingTouch(SeekBar seekBar) {
						// TODO Auto-generated method stub
						
					}
				});
				//TextView valorSeek = (TextView) ventana.findViewById(R.id.seekBar1);
				builder.setView(ventana)
				.setPositiveButton("Comprar", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						
						DrogasFragment.this.misDrogas.get(DrogasFragment.this.position).setCantidad(seekbar1.getProgress() +
								misDrogas.get(DrogasFragment.this.position).getCantidadInt());
						actualizarLista();
						double precioDroga = misDrogas.get(DrogasFragment.this.position).getPrecio();
						int cantidadComprada = Integer.parseInt(valorSeek.getText().toString());
						actualizarPisto(precioDroga * cantidadComprada);
					}
				});
				AlertDialog compra = builder.create();
				compra.show();
				
			}
		});
		
		AlertDialog dialog = builder.create();
		dialog.show();
		//Toast.makeText(getActivity(), "idk" + misDrogas.get(position).getNombre(), Toast.LENGTH_SHORT).show();
	}
	
	private void actualizarLista(){
		// Actualiza la lista de drogas con las cantidades compradas/vendidas.
		misDrogas = ld.getListaDrogas();
		setListAdapter(new DrogaArrayAdapter(getActivity(),misDrogas));
	}
	private void actualizarPisto(double pistoRestar){
		double pistoActual = ((PartidaActivity)getActivity()).juego.getPistoDouble();
		((PartidaActivity)getActivity()).actualizarPisto(pistoActual - pistoRestar);
	}
	
	public void actualizarYGenerarLista(){
		misDrogas = ld.getListaDrogasNuevosPrecios();
		setListAdapter(new DrogaArrayAdapter(getActivity(),misDrogas));
	}
	
	private int getMaximoComprable(Droga drogaComprable){
		double precioDroga = drogaComprable.getPrecio();
		double pisto = ((PartidaActivity)getActivity()).juego.getPistoDouble();
		int cantidadComprable = 0;
		while (pisto > precioDroga && cantidadComprable < 101){
			cantidadComprable += 1;
			pisto = pisto - precioDroga;
		}
		return cantidadComprable;
	}
	

	
	
	
	

}
