package com.hacktory.soyawars;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Random;

public class ListaDrogas {
	private ArrayList<Droga> lista = new ArrayList<Droga>();
	
	public ListaDrogas(){
		
	}
	//precioAnfetamina, precioCrack, precioCocaina, precioExtasis, precioHashis, precioHeroina, precioHongos, precioLSD, precioMarihuana, 
	//precioMetacualona, precioTenanfetamina;
	public ArrayList<Droga> generarDrogas(){
		this.lista.add(generar("Anfetamina",0,generarAnfetamina())); // speed 90 y 250. (250.00-90.00)+90.00;  //600 parciales unos majes estan locos
		this.lista.add(generar("Crack",0,generarCrack())); // 1.4k y 3k // (3000.00-1400.00)+1400.00;
		this.lista.add(generar("Cocaina",0,generarCocaina())); //cocaine 15k a 28k (28000.00-90.00)+15000.00;
		this.lista.add(generar("Extasis",0,generarExtasis())); //es como speed casi. un poco mas caras double. 180 a 600  (600.00-180.00)+180.00;
		this.lista.add(generar("Heroina",0,generarHeroina())); //heroin 4k a 11k (11900.00-4000.00)+4000.00;
		this.lista.add(generar("Hongos",0,generarHongos())); //shrooms  500 a 1300 (1300.00-500.00)+500.00;
		this.lista.add(generar("LSD",0,generarLSD())); //acid 1k a 4.5k (4500.00-1000.00)+1000.00;
		this.lista.add(generar("Marihuana",0,generarMarihuana())); //weed  300 a 900 (900.00-300.00)+300.00;
		this.lista.add(generar("Metacualona",0,generarMetacualona())); //ludes 16 a 57  (57.00-16.00)+16.00;
		
		return this.lista;
	}
	public ArrayList<Droga> getListaDrogas(){
		return this.lista;
	}
	
	public ArrayList<Droga> getListaDrogasNuevosPrecios(){
		//int len = this.lista.size();
		//Droga nuevaDroga;
		//for (int i = 0; i < len; i++){
		this.lista.get(0).setPrecio(generarAnfetamina());
		this.lista.get(1).setPrecio(generarCrack());
		this.lista.get(2).setPrecio(generarCocaina());
		this.lista.get(3).setPrecio(generarExtasis());
		this.lista.get(4).setPrecio(generarHeroina());
		this.lista.get(5).setPrecio(generarHongos());
		this.lista.get(6).setPrecio(generarLSD());
		this.lista.get(7).setPrecio(generarMarihuana());
		this.lista.get(8).setPrecio(generarMetacualona());
			//this.lista.set(i, object)
		//}
		
		return this.lista;
	}
	//public ArrayList<Droga> actualizarCantidadDrogas(){
		
	//}
	
	private Double generarAnfetamina(){
		Random gen = new Random();
		double num = gen.nextDouble()*(250.00-90.00)+90.00;
		return num;
	}
	private Double generarCocaina(){
		Random gen = new Random();
		double num = gen.nextDouble()*(28000.00-15000.00)+15000.00;
		return num;
	}
	private Double generarExtasis(){
		Random gen = new Random();
		double num = gen.nextDouble()*(600.00-180.00)+180.00;
		return num;
	}
	private Double generarHeroina(){
		Random gen = new Random();
		double num = gen.nextDouble()*(11900.00-4000.00)+4000.00;
		return num;
	}
	private Double generarHongos(){
		Random gen = new Random();
		double num = gen.nextDouble()*(1300.00-500.00)+500.00;
		return num;
	}
	private Double generarMarihuana(){
		Random gen = new Random();
		double num = gen.nextDouble()*(900.00-300.00)+300.00;
		return num;
	}
	private Double generarMetacualona(){
		Random gen = new Random();
		double num = gen.nextDouble()*(57.00-16.00)+16.00;
		return num;
	}
	
	private Double generarLSD(){
		//DecimalFormat form = new DecimalFormat("0.00");
		Random gen = new Random();
		double num = gen.nextDouble()*(4500.00-1000.00)+1000.00;
		//return form.format(precio);
		return num;
	}
	private Double generarCrack(){
		Random gen = new Random();
		double num = gen.nextDouble()*(3000.00-1400.00)+1400.00;
		return num;
	}
	
	private Droga generar(String s, int i, double f){
		return new Droga(s,i,f);
	}
	
}
