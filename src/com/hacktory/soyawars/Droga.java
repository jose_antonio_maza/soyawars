package com.hacktory.soyawars;

import java.text.DecimalFormat;

public class Droga {
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public boolean isSel() {
		return sel;
	}

	public void setSel(boolean sel) {
		this.sel = sel;
	}

	public String getCantidad() {
		return Integer.toString(cantidad);
	}
	public int getCantidadInt(){
		return this.cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	private String nombre;
	private boolean sel;
	private int cantidad;
	private double precio;
	
	public String getPrecioFormato() {
		DecimalFormat form = new DecimalFormat("0.00");
		return form.format(precio);
	}
	
	public Droga(String nombre, int cantidad, double precio){
		this.nombre = nombre;
		this.cantidad = cantidad;
		this.precio = precio;
	}
	
	
	
	
	
}
